import numpy as np
import matplotlib.pyplot as plt
from brian2 import ms, mV, nS

# Functions necessary to make the figure of the article


def fsave(save):
    """Save a figure with my pref"""
    if save:
        plt.tight_layout()
        plt.savefig(save, dpi=300)
        plt.close()
    else:
        plt.show()


def adjust_spines(ax, spines):
    """Remove the spines from a matplotlib graphics.
    taken from matplotlib gallery anonymous author.

    parameters
    ----------
    ax: a matplolib axes object
        handler of the object to work with
    spines: list of char
        location of the spines

    """
    for loc, spine in ax.spines.items():
        if loc in spines:
            pass
        else:
            spine.set_color('none')
            # don't draw spine
            # turn off ticks where there is no spine

    if 'left' in spines:
        ax.yaxis.set_ticks_position('left')
    else:
        # no yaxis ticks
        ax.yaxis.set_ticks([])

    if 'bottom' in spines:
        ax.xaxis.set_ticks_position('bottom')
    else:
        # no xaxis ticks
        ax.xaxis.set_ticks([])


def v_short(t, v_clusts, v_scats, v_lin, weights, example_weights, save=None):
    """Trace the voltage of a given section """
    t = t/ms
    fig, ax = plt.subplots(ncols=len(example_weights), nrows=1,
                           figsize=(2.7, 1.5))
    # Examples:
    for i,  w in enumerate(example_weights):
        w_idx = np.argmin(np.abs(weights - w))
        vc = v_clusts[w_idx]
        vs = v_scats[w_idx]
        vlin = v_lin[w_idx]
        ax[i].plot(t, vc/mV, color=(0, 0.5, 0.5), lw=2)
        ax[i].plot(t, vs/mV, color="black", lw=2)
        ax[i].plot(t, 2*(vlin/mV + 65) - 65, ':', color="gray", lw=2)
        ax[i].set_title(f'{weights[w_idx]/nS:.0f} nS')
        ax[i].set_ylim(-70, -40)
        if i == 0:
            adjust_spines(ax[i], ["left", "bottom"])
        else:
            adjust_spines(ax[i], ["bottom"])
    ax[0].set_ylabel("Voltage (mV)")
    ax[1].set_xlabel("Time (ms)")

    fsave(save)


def max_v_weight_dependence(v_clusts, v_scats, weights, example_weights, save=None):
    fig, ax = plt.subplots(figsize=(1.8, 1.5))
    # Maximum voltage over weights
    ax.plot(weights/nS, [max(vc/mV) for vc in v_clusts],
            color=(0, 0.5, 0.5), lw=2)
    ax.plot(weights / nS, [max(vs / mV) for vs in v_scats],
            color='black', lw=2)

    ax.set(xlabel='Total weight (nS)',
           ylabel='Voltage (mV)',
           xscale='log',
           ylim=(-70, -40))
    adjust_spines(ax, ["left", "bottom"])

    # Dots for examples
    for ex_weight in example_weights:
        w_idx = np.argmin(np.abs(weights - ex_weight))
        vc = v_clusts[w_idx]
        vs = v_scats[w_idx]
        ax.plot([ex_weight/nS], [max(vc/mV)], 'o', color=(0, 0.5, 0.5), lw=2)
        ax.plot([ex_weight/nS], [max(vs/mV)], 'o', color='black', lw=2)

    ax.set_xticks([10, 100])
    fsave(save)


def v_long(t, ipt, outs, colors, save=None):
    t = t/ms
    ymin = -90
    ymax = 50
    fig, ax = plt.subplots(ncols=1, nrows=6, figsize=(4.5, 5))
    ax[0].eventplot(ipt/ms, color=["black", "green", "blue"])
    ax[0].set_xlim((0, 250))
    adjust_spines(ax[0], [])
    for i in range(4):
        ax[1].plot(t, outs[i]/mV, color=colors[i])
    adjust_spines(ax[1], ["left"])
    ax[1].set_ylim(ymin, ymax)
    ax[1].set_xlim(0, 250)

    for i in range(4, 7):
        j = i-2
        ax[j].plot(t, outs[i]/mV, color=colors[i])
        adjust_spines(ax[j], ["left"])
        if j == 3:
            ax[j].set_ylabel("Voltage (mV)")
        ax[j].set_ylim(ymin, ymax)
        ax[j].set_xlim(0, 250)

    ax[-1].plot(t, outs[7]/mV, color=colors[7])
    adjust_spines(ax[-1], ["left", "bottom"])
    ax[-1].set_xlim(0, 250)
    ax[-1].set_ylim(ymin, ymax)

    ax[-1].set_xlabel("Time (ms)")
    fsave(save)


def v_binary_spikes(binary_patterns, v_mon, spike_mon, save=None):
    fig, axes = plt.subplots(3, 1, sharex=True,
                             gridspec_kw={'height_ratios': [1, 1, 3]},
                             figsize=(4.5, 4))
    colors = ['black', 'green', 'blue']
    for idx, (pattern, color) in enumerate(zip(binary_patterns, colors)):
        axes[0].eventplot(pattern/ms, lineoffsets=3-idx, color=color)
    axes[0].axis('off')
    axes[0].set_ylim(0, 4)
    for idx, (pattern, color) in enumerate(zip(binary_patterns, colors)):
        for t_start in np.arange(20)*25*ms:
            has_spike = np.any((pattern >= t_start) & (pattern < t_start+25*ms))
            text = '1' if has_spike else '0'
            axes[1].text(t_start/ms, 3-idx, text, horizontalalignment='center',
                         verticalalignment='center', color=color)
    axes[1].set_ylim(0, 4)
    axes[1].axis('off')
    axes[2].plot(v_mon.t/ms, v_mon[0].v/mV, color='black')
    # Connect spikes to the pattern that caused them
    for spike in spike_mon.t/ms:
        from matplotlib.patches import ConnectionPatch
        con = ConnectionPatch(xyA=(spike, np.max(v_mon[0].v/mV)),
                              xyB=((spike // 25.0)*25, 0),
                              coordsA="data", coordsB="data",
                              axesA=axes[2], axesB=axes[1],
                              color="gray", linestyle=':')
        axes[2].add_artist(con)
    axes[2].spines['top'].set_visible(False)
    axes[2].spines['right'].set_visible(False)
    axes[2].set_xlabel("Time (ms)")
    axes[2].set_ylabel("Voltage (mV)")
    fsave(save)
