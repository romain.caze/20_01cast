import argparse
import os

import numpy as np

import brian2 as br2
from brian2 import ms, um, cm, mV, nS, siemens, ufarad
from brian2.core.functions import timestep

import plot


def inp_spike_trains(n_spikes, n_trains, duration):
    """Build the input spike trains

    Parameters
    ----------
    n_spikes : int
        number of input spikes per spike train
    n_trains : int
        number of spike trains
    duration: br2.Quantity
        total duration

    Returns
    -------
    times : list
        List of ``n_trains`` spike trains with ``n_spikes`` each
    """
    duration_steps = int(round(duration/br2.defaultclock.dt))
    times = []
    for i in range(n_trains):
        spk_t_list = []
        for j in range(n_spikes):
            spk_t = np.random.randint(duration_steps)
            while spk_t in spk_t_list:  # To avoid in_spikes in the same dt
                spk_t = np.random.randint(duration_steps)
            spk_t_list.append(spk_t)
        times.append(spk_t_list*br2.defaultclock.dt)
    return times


def inp_spike_patterns(m=3, patterns=10, width=1 * ms, pause=10 * ms):
    """
    Create binary spike patterns.

    Parameters
    -----------
    m: int, optional
        Length of every binary "word"
    patterns: int, optional
        Number of patterns to generate
    width: br2.Quantity
        Jitter (spike times are generated within this interval)
    pause: br2.Quantity
        Pause between spike patterns
        , every ``pause``

    Returns
    -------
    times : list
        List of ``m`` spike trains
    """
    times = [[] for _ in range(m)]
    for pattern in range(patterns):
        spiking = np.flatnonzero(np.random.random(size=m) < 0.5)
        spike_times = pause*pattern + np.random.random(size=len(spiking))*width
        for idx, st in zip(spiking, spike_times):
            times[idx].append(st)
    return [br2.Quantity(t) for t in times]


def run(inp_spikes, duration, weights, passive, model='HH', VT=-50 * mV):
    """Run the simulations with the given weights.
    inp_spikes: list
        List of input spike trains for each synapse
    duration : br2.Quantity
        Duration of the simulation
    weights : list or br2.Quantity
        Weights for the synapses
    passive : boolean
        Whether the neuron is non-spiking.
    model : 'HH' or 'LIF', optional
        The neuron model to use (Hodgkin-Huxley like or leaky integrate-and-fire)
    VT : br2.Quantity, optional
        Model parameter setting the excitability of the model (threshold in LIF model)

    Returns
    -------
    state_mon, spike_mon:
        Tuple of `StateMonitor` and `SpikeMonitor`
    """

    weights = br2.Quantity(weights)

    net = br2.Network()
    # Generate the two Poissonian inputs
    # Input spikes individual, clustered and scattered
    indices = []
    for i, time in enumerate(inp_spikes):
        indices += [i for _ in range(len(time))]
    inp = br2.SpikeGeneratorGroup(len(inp_spikes), indices,
                                  [t for spikes in inp_spikes for t in spikes])
    net.add(inp)

    # Morphology of a bipolar neuron
    morpho = br2.Soma(10*um)
    d_l = 400*um
    morpho["dend1"] = br2.Cylinder(diameter=0.4*um, length=d_l, n=4)
    morpho["dend2"] = br2.Cylinder(diameter=0.4*um, length=d_l, n=4)
    neuron = create_neuron(morpho, network=net, model=model, passive=passive,
                           VT=VT)

    syn = br2.Synapses(inp, neuron, 'w : siemens (constant)', on_pre='gs += w',
                       name='synapses')

    # Set the connections (first on dendrite one, all others on dendrite 2)
    syn.connect(i=0, j=morpho.dend1[(d_l-1*um)])
    syn.connect(i=np.arange(1, len(weights)), j=morpho.dend2[(d_l - 1*um)])
    syn.w[:] = weights
    net.add(syn)

    # Record the voltage and the spikes
    mon = br2.StateMonitor(neuron, 'v', record=True)
    spike_mon = br2.SpikeMonitor(neuron)
    net.add(mon, spike_mon)
    net.run(duration, namespace={})
    return mon, spike_mon


def create_neuron(morpho, network, passive=False, model='HH', VT=-50*mV):
    '''Create the neuron model

    Parameters
    ----------
    morpho: br2.Morphology
        The neuron morphology
    network: br2.Network
        The `Network` (the generated `NeuronGroup` will be added to the network)
    passive: boolean, optional
        Whether the neuron should be non-spiking
    model: 'HH' or 'LIF', optional
        The neuron model to use (Hodgkin-Huxley like or leaky integrate-and-fire)
    VT : br2.Quantity, optional
        Model parameter setting the excitability of the model (threshold in LIF model)

    Returns
    -------
    neuron : br2.NeuronGroup
    '''
    # Biophysical model: set both the neuron and synapses biophysics
    if model == 'HH':
        eqs = '''Im = gL*(EL-v)+gNa*m**3*h*(ENa-v)+gK*n**4*(EK-v) : amp/meter**2
                Is = gs*(Es-v) : amp (point current)
                dgs/dt = -gs/taus : siemens
                dm/dt = alpha_m*(1-m) - beta_m*m : 1
                alpha_m = 0.32*(mV**-1)*4*mV/exprel((13.*mV-v+VT)/(4*mV))/ms : second**-1
                beta_m = 0.28*(mV**-1)*5*mV/exprel((v-VT-40.*mV)/(5*mV))/ms : second**-1
                dn/dt = alpha_n*(1.-n) - beta_n*n : 1
                alpha_n = 0.032*(mV**-1)*5*mV/exprel((15.*mV-v+VT)/(5*mV))/ms : second**-1
                beta_n = .5*exp((10.*mV-v+VT)/(40.*mV))/ms : second**-1
                dh/dt = alpha_h*(1.-h) - beta_h*h : 1
                alpha_h = 0.128*exp((17.*mV-v+VT)/(18.*mV))/ms : second**-1
                beta_h = 4./(1+exp((40.*mV-v+VT)/(5.*mV)))/ms : second**-1
                gNa : siemens/meter**2 (constant)
                gK : siemens/meter**2 (constant)'''
        # Channels
        parameters = {'Cm': 1*ufarad*cm**-2,
                      'gL': 5e-5*siemens*cm**-2,
                      'EL': -65*mV,
                      'EK': -90*mV,
                      'ENa': 50*mV,
                      'VT': VT,
                      # synaptic parameters
                      'Es': 0 * mV,
                      'taus': 1 * ms
                      }
        neuron = br2.SpatialNeuron(morphology=morpho, model=eqs,
                                   method='exponential_euler',
                                   threshold='v > -20*mV',
                                   refractory='v > -20*mV',
                                   threshold_location=0,  # soma
                                   namespace=parameters,
                                   name='neuron')
        neuron.v = 'EL'
        if not passive:
            neuron.main.gNa = '2000 * gL'
            neuron.main.gK = '600 * gL'
    elif model == 'LIF':
        eqs = '''
                Im = gL*(EL-v) : amp/meter**2
                Is = 0.175*gs*(Es-v) : amp (point current)
                dgs/dt = -gs/taus : siemens
        '''
        parameters = {'Cm': 1 * ufarad * cm ** -2,
                      'gL': 5e-5 * siemens * cm ** -2,
                      'EL': -65 * mV,
                      'VT': VT,
                      # synaptic parameters
                      'Es': 0 * mV,
                      'taus': 1 * ms
                      }
        if passive:
            threshold = 'False'
        else:
            threshold = 'v > VT'

        # The reset makes the membrane potential jump *up*, as a crude
        # approximation of an action potential (that can then propagate to the
        # connected compartments). It will be reset to the resting potential
        # via a delayed synapse (see below)
        neuron = br2.SpatialNeuron(morphology=morpho, model=eqs,
                                   method='exact',
                                   threshold=threshold,
                                   refractory=threshold,
                                   threshold_location=0,  # soma
                                   reset='v = -20*mV',
                                   namespace=parameters,
                                   name='neuron')
        neuron.v = 'EL'
        if not passive:
            reset_AP = br2.Synapses(neuron, neuron, on_pre='v_post = EL',
                                    delay=1*ms, namespace=parameters,
                                    name='reset_AP')
            reset_AP.connect(i=0, j=0)
            network.add(reset_AP)
    else:
        raise TypeError('Unknown model \'{}\''.format(model))
    network.add(neuron)
    return neuron


def trim(monitor, start_time):
    """Trim the monitor time and voltage of a certain section"""
    dt = br2.defaultclock.dt
    start_step = timestep(start_time, dt)
    return monitor.t[start_step:], monitor.v[:, start_step:]


def v_traces(inp, spike_time, weight, passive, model='HH', init_time=25 * ms):
    """Generate voltage traces for scattered and clustered stimulation"""
    mon_clust, _ = run(inp, init_time + spike_time, [weight, 0*nS], passive, model=model)
    mon_scat, _ = run(inp, init_time + spike_time, [weight / 2., weight / 2.], passive, model=model)
    mon_lin, _ = run(inp, init_time + spike_time, [weight / 2., 0*nS], passive, model=model)
    # The voltage response for the grouped stim.
    t, v_clust = trim(mon_clust, spike_time)
    t, v_scat = trim(mon_scat, spike_time)
    t, v_lin = trim(mon_lin, spike_time)
    return t, v_clust, v_scat, v_lin


def v_traces_weights(inp, weights, model='HH'):
    """Generate voltage traces for a sample weight distribution"""
    duration = 250*ms
    mon, _ = run(inp, duration, weights, False, model=model)
    t = mon.t
    voltage = mon[0].v

    return t, voltage


if __name__ == "__main__":
    # Set seed and codegen target for reproducible simulations
    br2.prefs.codegen.target = 'cython'
    parser = argparse.ArgumentParser(description='Run the simulations to '
                                                 'reproduce the paper figures.')
    parser.add_argument('model', nargs='?', choices=['HH', 'LIF'],
                        default='HH', help='the model to use')
    parser.add_argument('--seed', nargs='?', type=int, default=2056375916, const=-1,
                        help='The random seed to use (omit to not use a seed)')
    args = parser.parse_args()
    if args.seed == -1:
        br2.seed()
    else:
        br2.seed(args.seed)

    passive = True
    spike_time = 10*ms
    inp = [[spike_time], [spike_time]]

    figdir = "Figs"
    if not os.path.exists(figdir):
        os.mkdir(figdir)

    print('EPSPs in passive model...')
    weights = [1, 2, 10, 20, 100, 200, 400]*nS
    example_weights = [2, 20, 200]*nS #We split the points to have additional values on the log scale
    v_clusts, v_scats, v_lin = [], [], []
    for weight in weights:
        print('.', end='')
        ts, vclu, vsca, vlin = v_traces(inp, spike_time, weight, passive,
                                        model=args.model)
        # plotting values at the soma (compartment 0)
        v_clusts.append(vclu[0])
        v_scats.append(vsca[0])
        v_lin.append(vlin[0])
    print()
    figname = os.path.join(figdir, 'epsp_' + args.model + '.png')
    plot.v_short(ts-10*ms, v_clusts, v_scats, v_lin, weights,
                 example_weights=example_weights,
                 save=figname)
    figname = os.path.join(figdir,
                           'epsp_weight_dependence_' + args.model + '.png')
    plot.max_v_weight_dependence(v_clusts, v_scats,  weights,
                                 example_weights=example_weights,
                                 save=figname)

    print('Integration of inputs distributed over dendrites...')
    weight = 20 * nS
    duration = 250*ms
    inp = inp_spike_trains(n_spikes=25, n_trains=3, duration=duration)
    all_weights, colors = zip(*[([0*nS, 0*nS, 0*nS], "gray"),
                                ([weight, 0*nS, 0*nS], "gray"),
                                ([0*nS, weight, 0*nS], "gray"),
                                ([0*nS, 0*nS, weight], "gray"),
                                ([weight, weight, 0*nS], "green"),
                                ([weight, 0*nS, weight], "blue"),
                                ([0*nS, weight, weight], (0, 0.5, 0.5)),
                                ([weight, weight, weight], "black")])
    v_out = []
    for weights in all_weights:
        t, volt = v_traces_weights(inp, weights, model=args.model)
        v_out.append(volt)
    figname = os.path.join(figdir, 'SLTU_in_out_' + args.model + '.png')
    plot.v_long(t, inp, v_out, colors, save=figname)

    print('Binary spike patterns')
    binary_patterns = inp_spike_patterns(patterns=20, pause=25 * ms)
    mon, spike_mon = run(binary_patterns, 500 * ms, [20, 20, 20]*nS,
                         passive=False, model=args.model, VT=-55*mV)
    figname = os.path.join(figdir, 'SLTU_spikes_' + args.model + '.png')
    plot.v_binary_spikes(binary_patterns, mon, spike_mon, save=figname)
