This repository contains the code used to generate the figures for the paper:

> Romain D. Cazé and Marcel Stimberg
> *Dendritic neuron can perform linearly separable computations with low resolution synaptic weights*
> F1000Research, September 2020

# Installation
The simulations have been run with Python 3.8, Brian 2.4, and matplotlib 3.3.1 for plotting. To install all the dependencies with [pip](https://medium.com/@boscacci/why-and-how-to-make-a-requirements-txt-f329c685181e) use:
```console
$ pip install -r requirements.txt
```

# Reproduce article's figures
To recreate the figures of the papers you can run:
```console
$ python simulate.py
```

The generated figures in the `Figs` directory correspond to the following figures in the paper:

* `epsp_HH.png` → Figure 4b
* `epsp_weight_dependence_HH.png` → Figure 4c
* `SLTU_in_out_HH.png` → Figure 5
* `SLTU_spikes_HH.png` → Figure 6

# Run experiments with new random numbers
By default, the simulations are run with the seed that was used to generate the figures in the
paper and should therefore generate the exact same figures (but see the
[notes on the reproducibility of random numbers](https://brian2.readthedocs.io/en/2.4/introduction/compatibility.html#random-numbers) in the Brian documentation.)

To run with new random numbers, you can set the `seed` argument
(or omit the value it to use a fresh seed for every run):
```console
$ python simulate.py --seed 12345
$ python simulate.py --seed  # different result every time
```

# Run experiments with different model
All simulations for the paper have been run with a Hodgkin-Huxley type model, but very similar
results can be obtained with different models. As a demonstration, the simulations can be run with
a leaky integrate-and-fire neuron model by setting the model (the default model is `HH`):
```console
$ python simulate.py LIF
```
